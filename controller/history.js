import BaseAPI from '.'
import { History } from '../model'
import { genUpdate, generateID, decodeToken } from '../common/function'
import CronServices from '../common/cron'

export default class HistoryServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await History.countDocuments({})
      res.json(payload)
    })
  }

  static async getByAdmin (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await History.find({})
      res.json(payload)
    })
  }

  static async getByUser (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const payload = await History.find({ createdUser })
      res.json(payload)
    })
  }

  static async getById (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { deviceId } = req.params
        const payload = await History.find({
          $or: [
            {
              deviceId, userCreate: createdUser
            },
            {
              deviceId, userCreate: { $ne: createdUser }, createdUser
            }
          ]
        }).sort({ createdAt: -1 })
        if (payload) {
          res.json(payload)
        } else {
          res.json(false)
        }
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const createField = genUpdate(req.body,
          ['deviceId', 'deviceName', 'signalType'])

        createField.createdUser = createdUser
        createField.id = generateID()
        console.log('HistoryServices -> create -> createField', createField)

        const payload = await History.create(createField)
        res.json(payload)
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async createLocal (payload) {
    try {
      const createField = genUpdate(payload,
        ['deviceId', 'deviceName', 'value', 'status', 'createdUser', 'signalTypeId', 'userCreate'])
      createField.id = generateID()
      const result = await History.create(createField)
      console.log('create history successfull')
      return result
    } catch (error) {
      console.log(error)
    }
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id, isActive } = req.body
        await History.findOneAndUpdate({ id }, { isActive }, { new: true }, async (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
