import BaseAPI from '.'
import { MyDevice, User } from '../model'
import { genUpdate, generateID, generateQrCode, generateKeyDevice } from '../common/function'
import DeviceServices from '../controller/device'

export default class MyDeviceServices {
  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const currentUser = await User.findOne({ id: createdUser }, { role: 1 })
      if (BaseAPI.checkRoleAdmin(currentUser)) {
        const { page, size } = req.query
        const total = await MyDevice.countDocuments({ })
        const payload = await MyDevice.find({ }).skip(parseInt(size) * (parseInt(page) - 1)).limit(parseInt(size)).sort({ createdAt: 1 })
        console.log('MyDeviceServices -> get -> payload', payload)
        res.json({
          result: payload,
          total
        })
      } else {
        res.json(false)
      }
    })
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const findUser = await User.findOne({ id: createdUser })
        if (BaseAPI.checkRoleAdmin(findUser)) {
          const { type } = req.body
          const qrCode = generateQrCode()
          const keyId = generateID()
          const token = generateKeyDevice(keyId)
          const image = process.env.QRCODE_SERVER + qrCode
          console.log('MyDeviceServices -> create -> image', image)
          const payload = await MyDevice.create({ id: qrCode, keyId, key: token, type, image })
          res.json(payload)
        } else {
          res.json(false)
        }
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async getDeviceByIdLocal (id) {
    const data = await MyDevice.findOne({ id })
    return data
  }

  static async updateStatusLocal (data) {
    try {
      const { id, status } = data
      const payload = await MyDevice.findOneAndUpdate({ id }, { status }, { new: true })
      if (!payload) return false
      return true
    } catch (err) {
      return false
    }
  }

  static async getById (req, res) {
    try {
      const { id } = req.params
      const findDevice = await MyDevice.findOne({ id })
      if (!findDevice) return res.json({ errMess: 'deviceNotFound' })
      if (findDevice.status === 'ACTIVE') return res.json({ errMess: 'deviceExistsUser' })
      res.json(findDevice)
    } catch (error) {
      res.status(500).send('error : ' + error)
    }
  }

  static async update (req, res) {
    try {
      const { id } = req.body
      console.log(req.body)
      const updateField = genUpdate(req.body,
        ['name', 'status'])
      await MyDevice.findOneAndUpdate({ id }, updateField, { new: true }, (err, result) => {
        if (result || !err) {
          MyDevice.updateMany({ MyDeviceId: id }, { MyDeviceName: req.body.name }, { new: true }, (errDeivce, data) => {
            if (!err || data) {
              console.log(true)
            } else {
              console.log(false)
            }
          })
          res.json(result)
        }
      })
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async resetDevice (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        console.log('resetDevice -> id', id)
        const payload = await MyDevice.findOneAndUpdate({ id }, { status: 'INACTIVE' }, { new: true })
        console.log('resetDevice -> payload', payload)
        if (!payload) return res.json(false)
        res.json(true)
        DeviceServices.resetDeviceLocal(payload.key, payload.keyId)
      } catch (err) {
        res.json(false)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const findUser = await User.findOne({ id: createdUser })
        if (BaseAPI.checkRoleAdmin(findUser)) {
          const { id, status } = req.query
          await MyDevice.findOneAndUpdate({ _id: id }, { isDevice: status }, { new: true }, (err, result) => {
            if (!err || result) {
              res.json(result)
            } else {
              res.json(false)
            }
          })
        } else {
          res.json(false)
        }
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
