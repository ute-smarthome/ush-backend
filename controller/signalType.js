import BaseAPI from '.'
import { SignalType } from '../model'
import { genUpdate, generateID } from '../common/function'

export default class SignalTypeServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await SignalType.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await SignalType.find({})
      res.json(payload)
    })
  }

  static async getById (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await SignalType.find({ id: req.params.id })
      res.json(payload)
    })
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const createField = genUpdate(req.body,
          ['name', 'status', 'value', 'unit'])
        createField.id = generateID()

        const payload = await SignalType.create(createField)
        res.json(payload)
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async update (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        const updateField = genUpdate(req.body,
          ['name', 'status', 'value', 'unit'])
        await SignalType.findOneAndUpdate({ id }, updateField, { new: true }, (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id, isActive } = req.body
        await SignalType.findOneAndUpdate({ id }, { isActive }, { new: true }, async (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
