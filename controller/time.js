import BaseAPI from '.'
import { Time } from '../model'
import CronServices from '../common/cron'
import { genUpdate, generateID } from '../common/function'

export default class TImeServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await Time.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const payload = await Time.find({ createdUser, status: { $nin: ['DELETED'] } })
      res.json(payload)
    })
  }

  static async getLocal () {
    const payload = await Time.find({ status: 'ACTIVE' })
    return payload
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { action, timeExpression, type } = req.body
        const payload = await Time.create({ id: generateID(), timeExpression: timeExpression, createdUser, type, action })
        console.log(payload)
        CronServices[type]({ key: payload.id, timeExpression: timeExpression, action })
        res.json(payload)
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async update (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        const updateField = genUpdate(req.body, ['action', 'type', 'timeExpression'])
        await Time.findOneAndUpdate({ id }, updateField, { new: true }, async (err, result) => {
          if (!err || result) {
            res.json(result)
            await CronServices.deleteJob(result.id)
            await CronServices[result.type]({ key: result.id, timeExpression: result.timeExpression, action: result.action })
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async reCancel (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        await Time.findOneAndUpdate({ id }, { status: 'ACTIVE' }, { new: true }, (err, result) => {
          if (!err || result) {
            res.json(true)
            CronServices[result.type]({ key: result.id, timeExpression: result.timeExpression, action: result.action })
          } else {
            res.json(false)
          }
        })
      } catch (err) {
        res.status(500).send('error : ' + err)
      }
    })
  }

  static async cancel (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        await Time.findOneAndUpdate({ id }, { status: 'INACTIVE' }, { new: true }, (err, result) => {
          if (!err || result) {
            res.json(true)
            CronServices.stopJob(result.id)
          } else {
            res.json(false)
          }
        })
      } catch (err) {
        res.status(500).send('error : ' + err)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.query
        await Time.findOneAndUpdate({ id }, { status: 'DELETED' }, { new: true }, async (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
