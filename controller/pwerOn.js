import { PowerOn } from '../model'
import { genereateID } from '../common/function'
import BaseAPI from './index'

export default class PowerOnServices {
  static async createLocal ({ key, status }) {
    const id = genereateID()
    const payload = PowerOn.create({ id, key, status })
    return !!payload
  }

  static async updateLocal (id, status) {
    try {
      const findData = await PowerOn.findOne({ id })
      if (!findData) return false
      findData.status = status
      findData.save()
    } catch (err) {
      return false
    }
  }

  static async deleteLocal (id, status) {
    const payload = await PowerOn.findOneAndUpdate({ id }, { status }, { new: true })
    if (!payload) return false
    return true
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id, status } = req.query

        const payload = await PowerOn.findOneAndUpdate({ id }, { status }, { new: true })
        if (!payload) return false
        return payload
      } catch (err) {
        res.json({ success: false, errMess: err })
      }
    })
  }
}
