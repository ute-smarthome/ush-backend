import BaseAPI from '.'
import { ContactUs, User } from '../model'
import { genUpdate } from '../common/function'

export default class ContactUsServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await ContactUs.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async (id) => {
      const currentUser = await User.findOne({ id }, { role: 1 })
      if (BaseAPI.checkRoleAdmin(currentUser)) {
        const { page, size } = req.query
        const total = await ContactUs.countDocuments({ status: 'ACTIVE' })
        const payload = await ContactUs.find({ status: 'ACTIVE' }).skip(parseInt(size) * (parseInt(page) - 1)).limit(parseInt(size)).sort({ createdAt: 1 })
        res.json({
          result: payload,
          total
        })
      } else {
        res.json(false)
      }
    })
  }

  static async create (req, res) {
    try {
      const { email } = req.body
      console.log('ContactUsServices -> create -> req.body', req.body)
      const createField = genUpdate(req.body,
        ['title', 'content'])
      createField.email = email || 'Anonymous User'
      const payload = await ContactUs.create(createField)
      res.json(payload)
    } catch (error) {
      res.send('error :' + error)
    }
  }

  static async update (req, res) {
    BaseAPI.authorizationAPI(req, res, async (id) => {
      try {
        const currentUser = await User.findOne({ id }, { role: 1 })
        if (BaseAPI.checkRoleAdmin(currentUser)) {
          const { id } = req.body
          const updateField = genUpdate(req.body,
            ['isHandling'])
          console.log('ContactUsServices -> update -> updateField', updateField)
          await ContactUs.findOneAndUpdate({ _id: id }, updateField, { new: true }, (err, result) => {
            if (result || !err) {
              res.json(result)
            } else {
              res.json(false)
            }
          })
        } else {
          res.json(false)
        }
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id, status } = req.query
        await ContactUs.findOneAndUpdate({ _id: id }, { status }, { new: true }, async (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
