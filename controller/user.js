import BaseAPI from '.'
import { User } from '../model'
import { fetchAPI, generateToken, convertPasswordHMAC256, genUpdate, lowerCase, generatePassword, sendEmail, generateOTPCode } from '../common/function'
import FB from 'fb'
import { userRole } from '../common/constants'

export default class UserServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await User.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async (id) => {
      const currentUser = await User.findOne({ id }, { role: 1 })
      if (BaseAPI.checkRoleAdmin(currentUser)) {
        const { page, size } = req.query
        const total = await User.countDocuments({ })
        const payload = await User.find({ }).skip(parseInt(size) * (parseInt(page) - 1)).limit(parseInt(size)).sort({ createdAt: 1 })
        res.json({
          result: payload,
          total
        })
      } else {
        res.json(false)
      }
    })
  }

  static async getById (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const payload = await User.find({ id: req.params.id, createdUser: createdUser })
      res.json(payload)
    })
  }

  static async checkUser (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.params
        const payload = await User.findOne({ id })
        if (payload) {
          res.json(true)
        } else {
          res.json(false)
        }
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async checkuserLocal (id) {
    const payload = await User.findOne({ id })
    return !!payload
  }

  static async postLoginPassword (req, res) {
    try {
      const { email, password, isLogin } = req.body
      console.log('UserServices -> postLoginPassword -> email, password', email, password)
      res.json(await UserServices.onCreateUser({ isLogin, email, password: convertPasswordHMAC256(password) }))
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async postLoginAdmin (req, res) {
    try {
      const { email, password, isLoginAdmin } = req.body
      res.json(await UserServices.onCreateUser({ isLoginAdmin, email, password: convertPasswordHMAC256(password) }))
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async postLoginFacebook (req, res) {
    try {
      const { token } = req.body
      FB.api('/me', { fields: ['id', 'name', 'email', 'link', 'picture.type(large)'], access_token: token }, async (response) => {
        const { id, name, email, picture } = response
        res.json(await UserServices.onCreateUser({ id, name, email, picture: (picture && picture.data && picture.data.url) ? picture.data.url : '' }))
      })
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async postLoginGoogle (req, res) {
    try {
      const { token } = req.body
      const response = await fetchAPI(`https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${token}`)
      res.json(await UserServices.onCreateUser(response))
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async onCreateUser (response) {
    return new Promise(async (resolve, reject) => {
      const { id, picture, password, email, isLogin, isLoginAdmin } = response
      console.log(response)
      const emailFormat = lowerCase(email)

      const oldUser = password ? await User.findOne({ id: emailFormat }) : await User.findOne({ id })

      const jwtToken = generateToken(id || emailFormat)
      if (oldUser) {
        if (isLogin || isLoginAdmin) {
          if (oldUser.password !== password) {
            resolve({ errMess: 'namePwNotFound' })
          } else {
            if (isLoginAdmin) {
              if (oldUser.role === userRole.admin) {
                await User.findOneAndUpdate({ id }, { image: picture })
                resolve(BaseAPI.verifyResult({ jwtToken, data: oldUser }))
              } else {
                resolve({ errMess: 'notAdmin' })
              }
            } else {
              if (!oldUser.isVerify) { resolve({ errMess: 'notVerify' }) }
              await User.findOneAndUpdate({ id }, { image: picture })
              resolve(BaseAPI.verifyResult({ jwtToken, data: oldUser }))
            }
          }
        } else {
          if (password) {
            resolve({ errMess: 'userExisted' })
          } else {
            await User.findOneAndUpdate({ id }, { image: picture })
            resolve(BaseAPI.verifyResult({ jwtToken, data: oldUser }))
          }
        }
      } else {
        if (isLogin) {
          resolve({ errMess: 'namePwNotFound' })
        } else {
          const stateCreate = {
            id: password ? emailFormat : id,
            firstName: response.family_name,
            lastName: response.given_name,
            locale: response.locale,
            email: emailFormat,
            image: picture
          }

          if (password) {
            stateCreate.password = password
          }
          const code = generateOTPCode()
          stateCreate.verifyCode = code
          const result = await User.create(stateCreate)
          const link = process.env.SERVER_URL + `Verify?email=${emailFormat}&code=${code}`
          if (result) sendEmail(emailFormat, { subject: 'USH Smarthome - Xác thực tài khoản', link }, 'signUp')
          resolve({ success: true, message: 'Đăng kí thành công, kiểm tra email của bạn để xác thực !' })
        }
      }
    })
  }

  static async resetPassword (req, res) {
    try {
      const { email } = req.body
      const emailFormat = lowerCase(email)
      const findUser = await User.findOne({ id: emailFormat })
      if (!findUser) return res.json({ success: false, message: 'Không tìm thấy người dùng trong hệ thống !' })
      const newPassword = generatePassword()
      console.log('resetPassword -> newPassword', newPassword)
      findUser.password = convertPasswordHMAC256(newPassword)
      findUser.save()
      sendEmail(emailFormat, { password: newPassword, subject: 'USH Smarthome - Mật khẩu mới' }, 'resetPassword')
      res.json({ success: true, message: 'Đã gửi thành công, kiểm tra mail của bạn !' })
    } catch (err) {
      res.json({ success: false, message: err })
    }
  }

  static async verify (req, res) {
    const { email, code } = req.query
    console.log('verify -> email, code', email, code)
    await User.findOneAndUpdate({ id: email, verifyCode: code }, { isVerify: true }, { new: true }, (err, result) => {
      if (!err || result) {
        res.json({ success: true, message: 'Xác thực thành công !' })
      } else {
        res.json({ success: false, message: 'Đã có lỗi xảy ra !' })
      }
    })
  }

  static async update (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { id, password } = req.body
        const updateField = genUpdate(req.body,
          ['fullName', 'locale', 'image', 'email', 'gender', 'phone', 'address', 'status'])
        if (password) {
          updateField.password = convertPasswordHMAC256(password)
        }
        await User.findOneAndUpdate({ $or: [{ id: id }, { id: createdUser }] }, updateField, { new: true }, (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async updateAuto (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const { isAuto } = req.body
      const payload = await User.findOneAndUpdate({ id: createdUser }, { isAuto }, { new: true })
      res.json(payload)
    })
  }

  static async getAutoLocal (id) {
    const isAuto = (await User.findOne({ id }, { isAuto: 1 })).isAuto
    console.log('getAutoLocal -> isAuto', isAuto)
    return isAuto
  }

  static async changePassword (req, res) {
    try {
      const { id, oldPassword, newPassword } = req.body

      await User.findOneAndUpdate(
        { id, password: convertPasswordHMAC256(oldPassword) }, { password: convertPasswordHMAC256(newPassword) }, { new: true }, (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async (id) => {
      try {
        const currentUser = await User.findOne({ id }, { role: 1 })
        if (BaseAPI.checkRoleAdmin(currentUser)) {
          const { id, status } = req.query
          console.log('delete -> id, status ', id, status)
          await User.findOneAndUpdate({ _id: id, role: { $nin: ['admin'] } }, { status }, { new: true }, async (err, result) => {
            if (result || !err) {
              res.json(result)
            } else {
              res.json(false)
            }
          })
        } else {
          res.json(false)
        }
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
