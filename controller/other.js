import BaseAPI from '.'
import { User, Device, Area, SignalType, History } from '../model'
import { genUpdate, generateID, convertPasswordHMAC256, generateKeyDevice } from '../common/function'
const XLSX = require('xlsx')
const formidable = require('formidable')

export default class OtherServices {
  static async resetDataUser (req, res) {
    try {
      User.deleteMany({}, (err, resultRemove) => {
        if (!err || resultRemove) {
          const form = new formidable.IncomingForm()
          form.parse(req, (err, fields, files) => {
            if (err) {
              console.log(err)
              res.json(false)
            }
            const f = files[Object.keys(files)[0]]
            const workbook = XLSX.readFile(f.path)
            const sheeNameList = workbook.SheetNames
            const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[0]])
            data.map(async (item) => {
              const { id, fullName, email, phone, address, image, password, role, gender } = item
              User.create({ id, fullName, email, phone, address, image, role, gender, password: convertPasswordHMAC256(password) })
            })
            res.json(data)
          })
        }
      })
    } catch (error) {
      res.send('error :' + error)
    }
  }

  static async resetDataDevice (req, res) {
    try {
      Device.deleteMany({}, (err, resultRemove) => {
        if (!err || resultRemove) {
          const form = new formidable.IncomingForm()
          form.parse(req, (err, fields, files) => {
            if (err) {
              console.log(err)
              res.json(false)
            }
            const f = files[Object.keys(files)[0]]
            const workbook = XLSX.readFile(f.path)
            const sheeNameList = workbook.SheetNames
            const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[2]])
            data.map(async (item) => {
              item.userId = item.userShareId !== undefined ? item.userShareId.split(',') : []
              item.key = generateKeyDevice(item.id)
              console.log(item)
              Device.create(item)
            })
            res.json(data)
          })
        }
      })
    } catch (error) {
      res.send('error :' + error)
    }
  }

  static async resetDataArea (req, res) {
    try {
      Area.deleteMany({}, (err, resultRemove) => {
        if (!err || resultRemove) {
          const form = new formidable.IncomingForm()
          form.parse(req, (err, fields, files) => {
            if (err) {
              console.log(err)
              res.json(false)
            }
            const f = files[Object.keys(files)[0]]
            const workbook = XLSX.readFile(f.path)
            const sheeNameList = workbook.SheetNames
            const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[1]])
            data.map(async (item) => {
              Area.create(item)
            })
            res.json(data)
          })
        }
      })
    } catch (error) {
      res.send('error :' + error)
    }
  }

  static async resetDataSignalType (req, res) {
    try {
      SignalType.deleteMany({}, (err, resultRemove) => {
        if (!err || resultRemove) {
          const form = new formidable.IncomingForm()
          form.parse(req, (err, fields, files) => {
            if (err) {
              console.log(err)
              res.json(false)
            }
            const f = files[Object.keys(files)[0]]
            const workbook = XLSX.readFile(f.path)
            const sheeNameList = workbook.SheetNames
            const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[3]])
            data.map(async (item) => {
              SignalType.create(item)
            })
            res.json(data)
          })
        }
      })
    } catch (error) {
      res.send('error :' + error)
    }
  }

  static async resetDataHistory (req, res) {
    try {
      History.deleteMany({}, (err, resultRemove) => {
        if (!err || resultRemove) {
          const form = new formidable.IncomingForm()
          form.parse(req, (err, fields, files) => {
            if (err) {
              console.log(err)
              res.json(false)
            }
            const f = files[Object.keys(files)[0]]
            const workbook = XLSX.readFile(f.path)
            const sheeNameList = workbook.SheetNames
            const data = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[4]])
            const dataSignal = XLSX.utils.sheet_to_json(workbook.Sheets[sheeNameList[3]])
            const data1 = dataSignal[0]
            const data2 = dataSignal[1]
            data.map(async (item) => {
              item.signalType = item.signal === 1 ? data1 : data2
              console.log(item)
              History.create(item)
            })
            res.json(data)
          })
        }
      })
    } catch (error) {
      res.send('error :' + error)
    }
  }
}
