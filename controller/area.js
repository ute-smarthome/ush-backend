import BaseAPI from '.'
import { Area, Device } from '../model'
import { genUpdate, generateID } from '../common/function'

export default class AreaServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await Area.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const payload = await Area.find({ createdUser, status: 'ACTIVE' })
      res.json(payload)
    })
  }

  static async getById (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await Area.find({ id: req.params.id })
      res.json(payload)
    })
  }

  static async getAreaLocal (createdUser) {
    return (await Area.find({ createdUser, status: 'ACTIVE' })) || []
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { name } = req.body
        const payload = await Area.create({ id: generateID(), name, createdUser })
        res.json(payload)
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async update (req, res) {
    try {
      const { id } = req.body
      console.log(req.body)
      const updateField = genUpdate(req.body,
        ['name', 'status'])
      await Area.findOneAndUpdate({ id }, updateField, { new: true }, (err, result) => {
        if (result || !err) {
          Device.updateMany({ areaId: id }, { areaName: req.body.name }, { new: true }, (errDeivce, data) => {
            if (!err || data) {
              console.log(true)
            } else {
              console.log(false)
            }
          })
          res.json(result)
        }
      })
    } catch (error) {
      res.status(500).send('error :' + error)
    }
  }

  static async blockArea (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { areaId, blockArea } = req.body
        const check = await Area.findOne({ id: areaId, createdUser })
        if (check) {
          await Device.updateMany({ areaId }, { blockDeivice: blockArea }, { new: true }, (err, result) => {
            if (!err || result) {
              res.json(result)
            // emit socket
            } else {
              res.json(false)
            }
          })
        }
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { id, status } = req.query
        await Area.findOneAndUpdate({ id, createdUser }, { status }, { new: true }, (err, result) => {
          if (!err || result) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
