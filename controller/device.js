import BaseAPI from '.'
import AreaServices from '../controller/area'
import { genUpdate, generateID, convertPasswordHMAC256, generateKeyDevice, convertVI, getLength, sendSMS, smsNexmo } from '../common/function'
import HistoryServices from '../controller/history'
import UserServices from '../controller/user'
import MyDeviceServices from '../controller/myDevice'
import SocketServices from '../common/socket'
import { Device } from '../model'

export default class DeviceServices {
  static async count (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await Device.countDocuments({})
      res.json(payload)
    })
  }

  static async get (req, res) {
    console.log(true)
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const finalData = []
        await Device.find({ $or: [{ createdUser }, { userId: createdUser }], status: 'ACTIVE' }, async (err, data) => {
          if (!err || data) {
            const areaList = await AreaServices.getAreaLocal(createdUser)
            const dataFromArea = areaList.map(async item => {
              return item
            })
            const promisAll = await Promise.all(dataFromArea)
            res.json({ areaList: finalData.concat(promisAll), data })
          } else {
            res.json(false)
          }
        })
      } catch (err) {
        res.status(500).send('error :' + err)
      }
    })
  }

  static async checkDeviceLocal (id, token) {
    const payload = await Device.find({ keyId: id, key: token })
    return (getLength(payload) > 0)
  }

  static async getShareDeviceByUser (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      console.log(createdUser)
      const payload = await Device.find({ userId: createdUser })
      res.json(payload)
    })
  }

  static async turnOffAllDeviceLocal (key) {
    try {
      const createdUser = (await Device.findOne({ key }, { createdUser: 1 })).createdUser
      console.log('turnOffAllDeviceLocal -> createdUser', createdUser)
      if (!(await UserServices.getAutoLocal(createdUser))) return false
      // const findAllKey = (await Device.updateMany({ createdUser }, { lastValue: 0, lastStatus: false }, { new: true })).map(item => item.key)
      // console.log('turnOffAllDeviceLocal -> findAllKey', findAllKey)
      const findAllDevice = await Device.find({ createdUser })
      findAllDevice.forEach(item => {
        item.lastStatus = false
        item.lastValue = 0
        item.save()
        SocketServices.emitOneSocket(item.key, { key: item.key, data: { type: 'controllDevice', deviceId: item.id, value: item.lastValue, status: item.lastStatus } })
      })
      return true
    } catch (err) {
      return false
    }
  }

  static async turnOffAllDeviceByVoice (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { powerAll } = req.body
        console.log('turnOffAllDeviceByVoice -> powerAll', powerAll)
        const findAllDevice = await Device.find({ createdUser })
        const arrId = findAllDevice.map(item => {
          item.lastStatus = powerAll
          item.lastValue = !powerAll ? 0 : item.lastValue
          SocketServices.emitOneSocket(item.key, { key: item.key, data: { type: 'controllDevice', deviceId: item.id, value: item.lastValue, status: item.lastStatus } })
          return item._id
        })
        const listId = await Promise.all(arrId)
        console.log('turnOffAllDeviceByVoice -> listId', listId)
        await Device.updateMany({ createdUser }, { lastStatus: false, lastValue: 0 }, { new: true })
        res.json(true)
      } catch (err) {
        res.json(false)
      }
    })
  }

  static async getShareDeviceOfUser (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const payload = await Device.find({
        $and: [
          { createdUser },
          { userId: { $exists: true, $not: { $size: 0 } } }
        ]
      })
      res.json(payload)
    })
  }

  static async getAllDeviceLocal (id) {
    const payload = await Device.find({
      $or: [
        { createdUser: id },
        { userId: id }
      ]
    })
    const arrKey = payload.map(item => { return item.key })

    return [...new Set(arrKey)]
  }

  static async getListDeviceByArea (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      const { areaId } = req.params
      const payload = await Device.find({
        $and: [{
          $or: [
            { createdUser },
            { userId: createdUser }
          ]
        },
        { areaId }
        ]
      })
      res.json(payload)
    })
  }

  static async getById (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      const payload = await Device.find({ id: req.params.id })
      res.json(payload)
    })
  }

  static async create (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { data } = req.body
        // const createField = genUpdate(req.body,
        //   ['name', 'signalTypeId', 'userId', 'areaId', 'areaName'])
        const keyId = generateID()
        const key = generateKeyDevice(keyId)
        const newData = data.map((item, index) => {
          item.createdUser = createdUser
          item.keyId = keyId
          item.key = key
          item.id = keyId + `_${index + 1}`
          item.searchName = convertVI(item.name)
          item.searchAreaName = convertVI(item.areaName)
          return item
        })
        console.log('create -> newData', newData)

        const payload = await Device.insertMany(newData)
        res.json(payload)
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }

  static async resetDeviceLocal (key, keyId) {
    const payload = await Device.deleteMany({ keyId })
    SocketServices.emitOneSocket(key, { key, data: { type: 'resetDevice', keyId } })
    console.log("resetDeviceLocal -> key, { key, data: { type: 'resetDevice', keyId } }", key, { key, data: { type: 'resetDevice', keyId } })
    return !payload
  }

  static async createFromMyDevice (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { data, keyId, key, id } = req.body
        console.log('createFromMyDevice -> data, keyId, key, id', data, keyId, key, id)
        const findMyDevice = await MyDeviceServices.getDeviceByIdLocal(id)
        if (!findMyDevice) return res.json({ errMess: 'deviceNotFound' })
        if (findMyDevice.status === 'ACTIVE') return res.json({ errMess: 'deviceHaveUser' })
        const newData = data.map((item, index) => {
          item.createdUser = createdUser
          item.id = keyId + `_${index + 1}`
          item.keyId = keyId
          item.key = key
          item.searchName = convertVI(item.name)
          item.searchAreaName = convertVI(item.areaName)
          return item
        })
        const payload = await Device.insertMany(newData)
        res.json(payload)
        MyDeviceServices.updateStatusLocal({ id, status: 'ACTIVE' })
      } catch (err) {
        res.status(500).send('error : ' + err)
      }
    })
  }

  static async addSearchName (req, res) {
    await Device.find({}, async (err, result) => {
      if (!err || result) {
        const promiseAll = result.map(async (item, index) => {
          item.searchName = convertVI(item.name)
          item.save()
          return item
        })
        const finalData = await Promise.all(promiseAll)
        if (finalData) {
          res.json(result)
        } else {
          res.json(false)
        }
      } else {
        res.json(false)
      }
    })
  }

  static async addSearchAreaName (req, res) {
    await Device.find({}, async (err, result) => {
      if (!err || result) {
        const promiseAll = result.map(async (item, index) => {
          item.searchAreaName = convertVI(item.areaName)
          item.save()
          return item
        })
        const finalData = await Promise.all(promiseAll)
        if (finalData) {
          res.json(result)
        } else {
          res.json(false)
        }
      } else {
        res.json(false)
      }
    })
  }

  // 1 bat 2 tat 3 tang 4 giam
  static async voiceControll (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      console.log('voiceControl -> createdUser', createdUser)
      const { name, command, type } = req.body
      if (command === 5) {
        return await DeviceServices.turnOffAllDeviceByVoice(req, res)
      }
      console.log('voiceControll -> name, command, type', name, command, type)
      const searchName = convertVI(name)
      if (type === 'deviceControl') {
        await Device.findOne({ status: 'ACTIVE', createdUser, searchName }, (err, result) => {
          console.log('voiceControll -> result', result)
          if (result && !err) {
            switch (command) {
            case 1:
              result.lastStatus = true
              result.lastValue = 0
              break
            case 2:
              result.lastStatus = false
              result.lastValue = 0
              break
            case 3:
              result.lastStatus = true
              result.lastValue = result.lastValue < 3 && result.lastValue + 1
              break
            case 4:
              result.lastStatus = true
              result.lastValue = result.lastValue > 0 && result.lastValue - 1
              break
            }
            result.save(async err => {
              if (!err) {
                res.json(true)
                // await HistoryServices.createLocal({ signalTypeId: result.signalTypeId, createdUser: result.createdUser, value: result.lastValue, status: result.lastStatus, deviceId: result.id, deviceName: result.name, userCreate: result.createdUser })
                SocketServices.emitOneSocket(result.key, { key: result.key, data: { type: 'controllDevice', deviceId: result.id, value: result.lastValue, status: result.lastStatus } })
              } else {
                res.json(false)
              }
            })
          } else {
            res.json(false)
          }
        })
      } else {
        let lastStatus, lastValue
        switch (command) {
        case 1:
          lastStatus = true
          lastValue = 0
          break
        case 2:
          lastStatus = false
          lastValue = 0
          break
        }
        const findDevice = await Device.find({ status: 'ACTIVE', createdUser, searchAreaName: searchName })
        if (getLength(findDevice) > 0 && [1, 2].includes(command)) {
          const promiseAll = findDevice.map(async (item, value) => {
            item.lastStatus = lastStatus
            item.lastValue = lastValue
            item.save(err => {
              if (!err) {
                // HistoryServices.createLocal({ signalTypeId: item.signalTypeId, createdUser: item.createdUser, value: item.lastValue, status: item.lastStatus, deviceId: item.id, deviceName: item.name, userCreate: item.createdUser })
                SocketServices.emitOneSocket(item.key, { key: item.key, data: { type: 'controllDevice', deviceId: item.id, value: item.lastValue, status: item.lastStatus } })
              } else {
                return false
              }
            })
            return item
          })
          const finalData = await Promise.all(promiseAll)
          if (getLength(finalData) > 0) {
            res.json(true)
          } else {
            res.json(false)
          }
        } else {
          res.json(false)
        }
      }
    })
  }

  static async update (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id } = req.body
        const updateField = genUpdate(req.body,
          ['name', 'status', 'signalTypeId', 'password', 'userId', 'areaId', 'areaName', 'powerOn', 'lastValue', 'lastStatus'])
        await Device.findOneAndUpdate({ id }, updateField, { new: true }, (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async updateStatusLocal (payload) {
    try {
      const { id } = payload
      const updateField = genUpdate(payload,
        ['lastValue', 'lastStatus', 'powerOn'])
      const device = await Device.findOneAndUpdate({ id }, updateField, { new: true }, async (err, result) => {
        if (result && !err) {
          await HistoryServices.createLocal({ signalTypeId: result.signalTypeId, createdUser: payload.createdUser, value: payload.lastValue, status: payload.lastStatus, deviceId: result.id, deviceName: result.name, userCreate: result.createdUser })
          return result
        } else {
          return false
        }
      })
      if (device) {
        if (device.lastValue > 55 && !device.isSendSMS) {
          device.isSendSMS = true
          device.save()
          const text = `${device.name} : ${device.lastValue} 
          Cảnh báo : nguy hiểm`
          smsNexmo('USH Smarthome', '84917623232', text)
        }
        if (device.lastValue < 55 && device.isSendSMS) {
          device.isSendSMS = false
          device.save()
          const text = `${device.name} : ${device.lastValue} 
          Cảnh báo : Đã về mức an toàn`
          smsNexmo('USH Smarthome', '84917623232', text)
        }
      }
    } catch (error) {
      return false
    }
  }

  static async updateConnectDeviceLocal (payload) {
    try {
      const { id, powerOn, isSendSMS } = payload
      console.log('updateConnectDeviceLocal -> id, powerOn ', id, powerOn)
      await Device.updateMany({ keyId: id }, { powerOn, isSendSMS }, { new: true }, (err, result) => {
        if (!err || result) {
          return true
        } else {
          return false
        }
      })
    } catch (err) {
      return false
    }
  }

  static async shareDevice (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { deviceId, userShareId, type } = req.body
        const checkUser = await UserServices.checkuserLocal(userShareId)
        if (!checkUser) {
          res.json({ success: false, message: 'Tài khoản không tồn tại !' })
        } else {
          await Device.findOne({ id: deviceId }, (err, device) => {
            if (!err || device) {
              if (device.userId.includes(userShareId)) {
                if (type) {
                  res.json({ success: false, message: 'Tài khoản này đã được share!' })
                } else {
                  const index = device.userId.findIndex(itm => itm === userShareId)
                  if (index !== -1) {
                    device.userId.splice(index, 1)
                    device.save(err => {
                      if (!err) {
                        res.json({ success: true, message: 'Xóa tài khoản thành công !', data: device })
                      }
                    })
                  } else {
                    res.json({ success: false, message: 'Đã có lỗi xảy ra !' })
                  }
                }
              } else {
                device.userId.push(userShareId)
                device.save((err) => {
                  if (!err) {
                    res.json({ success: true, message: 'Chia sẽ thành công !', data: device })
                  } else {
                    res.json({ success: false, message: 'Đã có lỗi xảy ra !' })
                  }
                })
              }
            } else {
              res.json({ success: false, message: 'Đã có lỗi xảy ra !' })
            }
          })
        }
      } catch (error) {
        res.status(500).json({ success: false, message: error })
      }
    })
  }

  static async blockDevice (req, res) {
    BaseAPI.authorizationAPI(req, res, async (createdUser) => {
      try {
        const { blockDevice, deviceId } = req.body
        await Device.findOneAndUpdate({ id: deviceId, createdUser }, { blockDevice }, { new: true }, (err, result) => {
          if (!err || result) {
            res.json(result)
          // emit Socket
          // blah
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.status(500).send('error :' + error)
      }
    })
  }

  static async delete (req, res) {
    BaseAPI.authorizationAPI(req, res, async () => {
      try {
        const { id, status } = req.query
        await Device.updateMany({ keyId: id }, { status }, { new: true }, async (err, result) => {
          if (result || !err) {
            res.json(result)
          } else {
            res.json(false)
          }
        })
      } catch (error) {
        res.send('error :' + error)
      }
    })
  }
}
