import http from 'http'
import cors from 'cors'
import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import { connectDatabase } from './common/connectDB'
import SocketServices from './common/socket'
import CronServices from './common/cron'
// Routes
import User from './routes/user'
import Area from './routes/area'
import Device from './routes/device'
import MyDevice from './routes/myDevice'
import Other from './routes/other'
import SignalType from './routes/signalType'
import History from './routes/history'
import Time from './routes/time'
import ContactUs from './routes/contactUs'
import i18n from 'i18n'
import { sendSMS, smsNexmo } from './common/function'
require('dotenv').config()

// Setup server express
const app = express()

app.use(morgan('dev'))
app.use(helmet())
app.use(cors())
app.use(bodyParser.json({ limit: '100mb' }))
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))
app.use(cookieParser())

app.get('/api/info', (req, res) => {
  res.json('welcome to ush !')
})

app.use('/api/user', User)
app.use('/api/area', Area)
app.use('/api/device', Device)
app.use('/api/other', Other)
app.use('/api/signaltype', SignalType)
app.use('/api/history', History)
app.use('/api/time', Time)
app.use('/api/myDevice', MyDevice)
app.use('/api/contactUs', ContactUs)

// error handler
app.use(function (err, req, res, next) {
  if (err.isBoom) {
    return res.status(err.output.statusCode).json(err.output.payload)
  }
})

i18n.configure({
  locales: ['en', 'vi'],
  directory: './locales'
})

const server = http.createServer(app)
SocketServices.createSocketIO(server)
// CronJob
CronServices.createCronJob()

// smsNexmo('USH Smarthome', '84917623232', 'Test SMS Chao Thay')

// setTimeout(() => {
//   CronServices.stopJob('test')
// }, 5000)

// Database connection
connectDatabase()

server.listen(process.env.PORT)

console.log('Starting Load: Adel tour server started at port ' + process.env.PORT)
