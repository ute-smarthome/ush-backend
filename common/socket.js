import socketio from 'socket.io'
import jwt from 'jsonwebtoken'
import { optionsSocket } from './constants'
import UserServices from '../controller/user'
import DeviceServices from '../controller/device'
import { isObject } from '../common/function'
import get from 'lodash/get'
let io
export default class SocketServices {
  static async createSocketIO (server) {
    io = socketio(server, optionsSocket)

    io.use((socket, next) => {
      const token = socket.handshake.query.token

      jwt.verify(token, process.env.SECRET_TOKEN, async (err, decoded) => {
        if (err) {
          console.log(false)
        } else {
          console.log(decoded)
          if (decoded.device) {
            // if (await DeviceServices.checkDeviceLocal(decoded.id, token)) {
            socket.token = token
            socket.isDevice = true
            socket.connectId = decoded.id
            socket.join(token)
            next()
            // }
          } else {
            if (await UserServices.checkuserLocal(decoded.id)) {
              const devices = await DeviceServices.getAllDeviceLocal(decoded.id)
              socket.isUser = true
              socket.token = token
              socket.connectId = decoded.id
              devices.map((item) => {
                socket.join(item)
              })
              next()
            }
          }
        }
      })
    })

    io.sockets.on('connection', (socket) => {
      if (socket.isDevice) {
        const roomId = Object.keys(socket.rooms)
        roomId.map((item) => {
          SocketServices.clientSendMessageToRoom(socket, item, { type: 'online', id: socket.connectId })
          DeviceServices.updateConnectDeviceLocal({ id: socket.connectId, powerOn: true, isSendSMS: false })
        })
      }
      socket.on('disconnect', () => {
        if (socket.isDevice) {
          SocketServices.emitOneSocket(socket.token, { type: 'offline', id: socket.connectId })
          DeviceServices.updateConnectDeviceLocal({ id: socket.connectId, powerOn: false, isSendSMS: false })
        }
        socket.removeAllListeners()
      })
      socket.on('message', payload => {
        console.log('SocketServices -> createSocketIO -> payload', payload)
        if (isObject(payload) && socket.isUser) {
          if (payload.data && payload.data.type === 'controllDevice') {
            SocketServices.clientSendMessageToRoom(socket, payload.key, payload)
            DeviceServices.updateStatusLocal({ createdUser: socket.connectId, id: payload.data.deviceId, lastValue: payload.data.value, lastStatus: payload.data.status })
          }
          if (payload.type === 'updateBlockDevice') {
            SocketServices.clientSendMessageToRoom(socket, payload.data.key, payload)
          }
        }

        if (isObject(payload) && socket.isDevice && payload.data && payload.data.type === 'controllDevice') {
          payload.data.deviceId = socket.connectId + '_' + payload.data.index
          SocketServices.clientSendMessageToRoom(socket, payload.key, payload)
          DeviceServices.updateStatusLocal({ createdUser: socket.connectId, id: socket.connectId + '_' + payload.data.index, lastValue: payload.data.value, lastStatus: payload.data.status })
        }
        if (isObject(payload) && socket.isDevice && payload.data && payload.data.type === 'sensorUpdate') {
          console.log('sensorUpdate')
          payload.data.deviceId = socket.connectId + '_' + payload.data.index
          SocketServices.clientSendMessageToRoom(socket, payload.key, payload)
          if (parseInt(get(payload, 'data.temp')) > 50) {
            // console.log('tat het')
            DeviceServices.turnOffAllDeviceLocal(payload.key)
          }
        }
      })
    })
  }

  static clientSendMessageToRoom (socket, id, payload) {
    socket.to(id).emit('message', { payload })
  }

  static emitOneSocket (id, payload) {
    io.to(id).emit('message', {
      payload
    })
  }

  static emitSocket (type, data) {
    io.emit('message', {
      type,
      data
    })
  }
}
