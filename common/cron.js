import HistoryServices from '../controller/history'
import DeviceServices from '../controller/device'
import { getCronExpression, getLength } from '../common/function'
import SocketServices from '../common/socket'
import TimeServices from '../controller/time'
const CronJobManager = require('cron-job-manager')
let cronManager
export default class CronServices {
  static async createCronJob () {
    cronManager = new CronJobManager()
    const arrTime = await TimeServices.getLocal()
    console.log('CronServices -> createCronJob -> arrTime', arrTime)
    if (getLength(arrTime) > 0) {
      arrTime.forEach(value => {
        CronServices[value.type]({ key: value.id, timeExpression: value.timeExpression, action: value.action })
      })
    }
    // cronManager.add('test', '* * * * * *', () => { console.log('test...') })
    // cronManager.start('test')
  }
  // const payload = await Time.create({ id: generateID(), timeExpression, createdUser, type, action })
  //       console.log(payload)
  //       CronServices[type]({ key: payload.id, timeExpression, action })

  static async oneTime (data) {
    console.log('CronServices -> oneTime -> data', data)
    const { key, timeExpression, action } = data
    if (cronManager.exists(key)) {
      cronManager.start(key)
    } else {
      cronManager.add(key, getCronExpression(timeExpression), () => {
        // do job
        console.log(action)
        SocketServices.emitOneSocket(action.key, action)
        // save device && history
        console.log('device && history')
        DeviceServices.updateStatusLocal({ createdUser: 'timeJob', id: action.data.deviceId, lastValue: action.data.value, lastStatus: action.data.status })
        // stop job
        cronManager.stop(key)
      })
      cronManager.start(key)
    }
  }

  static async loop (data) {
    const { key, timeExpression, action } = data
    if (cronManager.exists(key)) {
      cronManager.start(key)
    } else {
      cronManager.add(key, getCronExpression(timeExpression), () => {
        // do job
        console.log(action)
        SocketServices.emitOneSocket(action.key, action)
        // save device && history
        console.log('device && history')
        DeviceServices.updateStatusLocal({ createdUser: 'timeJob', id: action.data.deviceId, lastValue: action.data.value, lastStatus: action.data.status })
      })
      cronManager.start(key)
    }
  }

  static async stopJob (data) {
    console.log(data)
    cronManager.stop(data)
  }

  static async deleteJob (data) {
    cronManager.deleteJob(data)
  }
}
