import AreaServices from '../controller/area'
const express = require('express')
const router = express.Router()

router.get('/', AreaServices.get)
router.get('/me/:id', AreaServices.getById)

router.post('/', AreaServices.create)
router.put('/', AreaServices.update)
router.put('/block', AreaServices.blockArea)
router.delete('/', AreaServices.delete)

module.exports = router
