import OtherServices from '../controller/other'
const express = require('express')
const router = express.Router()

router.post('/user/reset', OtherServices.resetDataUser)
router.post('/device/reset', OtherServices.resetDataDevice)
router.post('/area/reset', OtherServices.resetDataArea)
router.post('/signalType/reset', OtherServices.resetDataSignalType)
router.post('/history/reset', OtherServices.resetDataHistory)

module.exports = router
