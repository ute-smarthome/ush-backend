import ContactUsServices from '../controller/contactUs'
const express = require('express')
const router = express.Router()

router.get('/', ContactUsServices.get)

router.post('/', ContactUsServices.create)
router.put('/', ContactUsServices.update)

router.delete('/', ContactUsServices.delete)

module.exports = router
