import DeviceServices from '../controller/device'
import { Device } from '../model'
const express = require('express')
const router = express.Router()

router.get('/', DeviceServices.get)
router.get('/me/:id', DeviceServices.getById)
router.get('/listByArea/:areaId', DeviceServices.getListDeviceByArea)
router.get('/listShareDevice', DeviceServices.getShareDeviceByUser)
router.get('/listDeviceShare', DeviceServices.getShareDeviceOfUser)
router.get('/addSearchName', DeviceServices.addSearchAreaName)

router.post('/allDevice', DeviceServices.turnOffAllDeviceByVoice)
router.post('/myDeivce', DeviceServices.createFromMyDevice)
router.post('/', DeviceServices.create)
router.post('/voice', DeviceServices.voiceControll)
router.put('/', DeviceServices.update)
router.put('/share', DeviceServices.shareDevice)
router.put('/block', DeviceServices.blockDevice)

router.delete('/', DeviceServices.delete)

module.exports = router
