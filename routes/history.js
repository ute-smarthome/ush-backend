import HistoryServices from '../controller/history'
const express = require('express')
const router = express.Router()

router.get('/byAdmin', HistoryServices.getByAdmin)

router.get('/', HistoryServices.getByUser)

router.get('/item/:deviceId', HistoryServices.getById)

router.post('/', HistoryServices.create)

router.delete('/', HistoryServices.delete)

module.exports = router
