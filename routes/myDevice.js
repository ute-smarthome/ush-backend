import MyDeviceServices from '../controller/myDevice'
const express = require('express')
const router = express.Router()

router.get('/', MyDeviceServices.get)
router.get('/me/:id', MyDeviceServices.getById)

router.post('/', MyDeviceServices.create)
router.post('/reset', MyDeviceServices.resetDevice)
router.put('/', MyDeviceServices.update)

router.delete('/', MyDeviceServices.delete)

module.exports = router
