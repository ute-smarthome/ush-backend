import SignalTypeServices from '../controller/signalType'
const express = require('express')
const router = express.Router()

router.get('/', SignalTypeServices.get)
router.get('/me/:id', SignalTypeServices.getById)

router.post('/', SignalTypeServices.create)
router.put('/', SignalTypeServices.update)

router.delete('/', SignalTypeServices.delete)

module.exports = router
