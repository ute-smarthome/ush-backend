
import TimeServices from '../controller/time'
const express = require('express')
const router = express.Router()

router.get('/', TimeServices.get)

router.post('/', TimeServices.create)
router.post('/cancel', TimeServices.cancel)
router.post('/active', TimeServices.reCancel)

router.put('/', TimeServices.update)
router.delete('/', TimeServices.delete)

module.exports = router
