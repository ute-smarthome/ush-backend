import { defaultModel, statusActive } from '../common/constants'

export default {
  id: defaultModel.stringUnique,
  key: defaultModel.string,
  status: { type: String, default: statusActive.inactive }
}
