import { defaultModel } from '../common/constants'

export default {
  id: defaultModel.stringUnique,

  name: defaultModel.string,
  value: defaultModel.string,
  status: defaultModel.string,
  unit: defaultModel.string

}
