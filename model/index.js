import mongoose from 'mongoose'

import UserSchema from './User'
import DeviceSchema from './Device'
import MyDeviceSchema from './MyDevice'
import AreaSchema from './Area'
import SignalTypeSchema from './SignalType'
import HistorySchema from './History'
import TimeSchema from './Time'
import ContactUsSchema from './ContactUs'
import PowerOnSchema from './PowerOn'

const Schema = mongoose.Schema

const createSchema = (schema) => {
  const model = new Schema(schema, { timestamps: true })
  return model
}

const User = mongoose.model('User', createSchema(UserSchema))
const Device = mongoose.model('Device', createSchema(DeviceSchema))
const MyDevice = mongoose.model('MyDevice', createSchema(MyDeviceSchema))
const Area = mongoose.model('Area', createSchema(AreaSchema))
const SignalType = mongoose.model('SignalType', createSchema(SignalTypeSchema))
const History = mongoose.model('History', createSchema(HistorySchema))
const Time = mongoose.model('Time', createSchema(TimeSchema))
const ContactUs = mongoose.model('ContactUs', createSchema(ContactUsSchema))
const PowerOn = mongoose.model('PowerOn', createSchema(PowerOnSchema))

export {
  User,
  Area,
  Device,
  MyDevice,
  SignalType,
  History,
  Time,
  ContactUs,
  PowerOn
}
