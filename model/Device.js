import { defaultModel, statusActive } from '../common/constants'

export default {
  id: defaultModel.stringUnique,

  keyId: defaultModel.string,
  signalTypeId: defaultModel.string,
  status: { type: String, default: statusActive.active },
  password: defaultModel.string,
  createdUser: defaultModel.string,
  name: defaultModel.string,
  searchName: defaultModel.string,
  searchAreaName: defaultModel.string,
  userId: defaultModel.array,
  areaId: defaultModel.string,
  type: defaultModel.string,
  areaName: defaultModel.string,
  powerOn: defaultModel.booleanFalse,
  key: defaultModel.string,
  lastValue: { type: Number, default: 0 },
  lastStatus: defaultModel.booleanFalse,
  blockDevice: defaultModel.booleanFalse,
  isSendSMS: defaultModel.booleanFalse

}
