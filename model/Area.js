import { defaultModel, statusActive } from '../common/constants'

export default {
  id: defaultModel.stringUnique,
  name: defaultModel.string,
  createdUser: defaultModel.string,
  status: { type: String, default: statusActive.active },
  isAtive:defaultModel.booleanFalse
}
