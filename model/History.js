import { defaultModel } from '../common/constants'

export default {

  signalTypeId: defaultModel.string,
  deviceId: defaultModel.string,
  deviceName: defaultModel.string,
  status: defaultModel.booleanFalse,
  value: { type: Number, default: 0 },

  startDate: defaultModel.date,
  endDate: defaultModel.date,

  createdUser: defaultModel.string,
  userCreate: defaultModel.string
}
