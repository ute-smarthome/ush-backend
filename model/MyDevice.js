import { defaultModel, statusActive } from '../common/constants'

export default {
  id: defaultModel.stringUnique,
  image: defaultModel.string,
  key: defaultModel.string,
  keyId: defaultModel.string,
  type: defaultModel.string,
  status: { type: String, default: statusActive.inactive },
  isDevice: defaultModel.booleanFalse

}
