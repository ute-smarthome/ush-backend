import { defaultModel, statusActive, timeType } from '../common/constants'

export default {
  id: defaultModel.stringUnique,

  action: defaultModel.object,
  timeExpression: defaultModel.array,
  type: { type: String, default: timeType.oneTime },

  createdUser: defaultModel.string,
  status: { type: String, default: statusActive.active }

}
