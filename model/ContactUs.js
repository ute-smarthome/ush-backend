import { defaultModel, statusActive } from '../common/constants'

export default {
  email: defaultModel.string,
  title: defaultModel.string,
  content: defaultModel.string,

  createdUser: defaultModel.string,
  status: { type: String, default: statusActive.active },
  isHandling: defaultModel.booleanFalse
}
